import {
  App,
  EffectScope,
  inject,
  hasInjectionContext,
  InjectionKey,
  Ref,
} from 'vue-demi'
import {
  StateTree,
  PiniaCustomProperties,
  _Method,
  Store,
  _GettersTree,
  _ActionsTree,
  PiniaCustomStateProperties,
  DefineStoreOptionsInPlugin,
  StoreGeneric,
} from './types'

/**
 * 当前激活的 pinia 实例
 * 
 * setActivePinia must be called to handle SSR at the top of functions like
 * `fetch`, `setup`, `serverPrefetch` and others
 */
export let activePinia: Pinia | undefined

/**
 * 设置当前激活的 pinia 实例
 * 
 * Sets or unsets the active pinia. Used in SSR and internally when calling
 * actions and getters
 *
 * @param pinia - Pinia instance
 */
// @ts-expect-error: cannot constrain the type of the return
export const setActivePinia: _SetActivePinia = (pinia) => (activePinia = pinia)

interface _SetActivePinia {
  (pinia: Pinia): Pinia
  (pinia: undefined): undefined
  (pinia: Pinia | undefined): Pinia | undefined
}

/**
 * Get the currently active pinia if there is any.
 */
export const getActivePinia = () =>
  (hasInjectionContext() && inject(piniaSymbol)) || activePinia

/**
 * Every application must own its own pinia to be able to create stores
 */
export interface Pinia {
  install: (app: App) => void

  /**
   * 包含所有 store 的状态
   * - { storeId: state }
   * 
   * root state
   */
  state: Ref<Record<string, StateTree>>

  /**
   * 给 pinia 安装插件
   * - 插件是安装到 pinia 上的，而不是安装到 store 上
   * - 插件会在每个 store 上运行，插件函数可以取得 store 的相关信息，其返回值将会合并到 store 中
   * - 插件可以读取到 store 的选项，可以添加新的或者修改（覆盖）旧的 action/state
   * - 插件可以调用 store 上的 $subscribe 和 $onAction api 进行事件订阅
   * 
   * Adds a store plugin to extend every store
   *
   * @param plugin - store plugin to add
   */
  use(plugin: PiniaPlugin): Pinia

  /**
   * 已安装的插件
   * 
   * Installed store plugins
   *
   * @internal
   */
  _p: PiniaPlugin[]

  /**
   * 使用此 pinia 的 app 实例（仅 vue3）
   * 
   * App linked to this Pinia instance
   *
   * @internal
   */
  _a: App

  /**
   * 整个 pinia 实例最高级别的 EffectScope（分离的）
   * - 各个 store 都会在此 EffectScope 中生成
   * 
   * Effect scope the pinia is attached to
   *
   * @internal
   */
  _e: EffectScope

  /**
   * 注册到此 pinia 实例上的所有 store
   * - { storeId: store }
   * 
   * Registry of stores used by this pinia.
   *
   * @internal
   */
  _s: Map<string, StoreGeneric>

  /**
   * Added by `createTestingPinia()` to bypass `useStore(pinia)`.
   *
   * @internal
   */
  _testing?: boolean
}

/**
 * 从 vue 根组件上向下注入（provide）pinia 实例时所使用的 key
 */
export const piniaSymbol = (
  __DEV__ ? Symbol('pinia') : /* istanbul ignore next */ Symbol()
) as InjectionKey<Pinia>

/**
 * pinia 插件函数所获取到的插件上下文
 * 
 * Context argument passed to Pinia plugins.
 */
export interface PiniaPluginContext<
  Id extends string = string,
  S extends StateTree = StateTree,
  G /* extends _GettersTree<S> */ = _GettersTree<S>,
  A /* extends _ActionsTree */ = _ActionsTree
> {
  /**
   * pinia instance.
   */
  pinia: Pinia

  /**
   * Current app created with `Vue.createApp()`.
   */
  app: App

  /**
   * Current store being extended.
   */
  store: Store<Id, S, G, A>

  /**
   * Initial options defining the store when calling `defineStore()`.
   */
  options: DefineStoreOptionsInPlugin<Id, S, G, A>
}

/**
 * Pinia 插件
 * - 插件其实就是一个函数
 * 
 * Plugin to extend every store.
 */
export interface PiniaPlugin {
  /**
   * Plugin to extend every store. Returns an object to extend the store or
   * nothing.
   *
   * @param context - Context
   */
  (context: PiniaPluginContext): Partial<
    PiniaCustomProperties & PiniaCustomStateProperties
  > | void
}

/**
 * Plugin to extend every store.
 * @deprecated use PiniaPlugin instead
 */
export type PiniaStorePlugin = PiniaPlugin
